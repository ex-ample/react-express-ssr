import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter as Router } from 'react-router-dom';
import App from '../client/components/app.js';

export const staticRenderer = (req, context) => {
    return renderToString(
	<Router location={req.url} context={context || {}}>
	    <App />
	</Router>
    );
};

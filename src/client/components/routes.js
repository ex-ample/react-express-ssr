import Home from './home.js';
import About from './about.js';

const Routes = [
    {
	url: "/",
	exact: true,
	component: Home
    },
    {
	url: "/about",
	exact: false,
	component: About
    }
];

export const menuLinks = [
    {
	url: "/",
	menuText: "Home"
    },
    {
	url: "/about",
	menuText: "About"
    }
];

export default Routes;

import React, { Fragment } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { menuLinks} from './routes.js';

const RenderMenuLink = ({list = {}}) => {
    return list.map((menu, index) => <NavLink key={index} to={menu.url}>{menu.menuText}</NavLink>);
};



const Header = () => {
    return(
	<div>
	    <RenderMenuLink list={menuLinks}/>
	</div>
    );
};

export default Header;

/* const styles = {
 *     link: {
 * 	color: "red"
 *     }
 * };*/

const devMode = process.env.NODE_ENV !== 'production';

const path = require("path");
const clientEntry = path.resolve(__dirname, 'src', 'client', 'index.js');
const rendererEntry = path.resolve(__dirname, 'src', 'server', 'renderer.js');
const clientOutput = devMode ? path.resolve(__dirname, 'dev') : path.resolve(__dirname, 'dist');
const rendererOutput = path.resolve(__dirname, 'server', 'build');
const jsPublicPath = '/assets/js/';
const cssPublicPath = '/assets/css/';

module.exports = [
    {
	mode: process.env.NODE_ENV,
	entry: {
	    app: clientEntry
	},
	output: {
	    filename: devMode ? "assets/js/[name].js" : "assets/js/[name]-[hash].js",
	    chunkFilename: devMode ? "assets/js/[name].js" : "assets/js/[name]-[hash].js",
	    path: clientOutput,
	    publicPath: jsPublicPath,
	},
	module: {
	    rules: [
		{
		    test: /\.(js|jsx)$/,
		    exclude: /node_modules/,
		    use: {
			loader: "babel-loader"
		    }
		},
		{
		    test: /\.css$/,
		    exclude: /node_modules/,
		    use: [
			'style-loader',
			'css-loader'
		    ]
		}
	    ]
	}
    },
    {
	mode: process.env.NODE_ENV,
	entry: {
	    renderer: rendererEntry
	},
	output: {
	    filename: "[name].js",
	    chunkFilename: "[name].js",
	    path: rendererOutput,
	    libraryTarget: "commonjs"
	},
	module: {
	    rules: [
		{
		    test: /\.(js|jsx)$/,
		    exclude: /node_modules/,
		    use: {
			loader: "babel-loader"
		    }
		},
		{
		    test: /\.css$/,
		    exclude: /node_modules/,
		    use: ['css-loader']
		}
	    ]
	}
    }
];

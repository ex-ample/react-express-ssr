const fn = require('fn-js');


const createLinks   = (links = [])   => fn.reduce((init, link)   => `${init}\n    <link rel='stylesheeet' src='${link}'/>`, links, ' ');
const createScripts = (scripts = []) => fn.reduce((init, script) => `${init}\n    <script type='text/javascript' src='${script}'></script>`, scripts, ' ');


const createMetas = ({plain = [], fb = [], tw = []}) => {
    const plainMetas = fn.reduce((init, {name, content}) => `${init}\n    <meta name='${name}' content='${content}'>`, plain, ' ');
    const fbMetas    = fn.reduce((init, {prop, content}) => `${init}\n    <meta property='og:${prop}' content='${content}'>`, fb, ' ');
    const twMetas    = fn.reduce((init, {name, content}) => `${init}\n    <meta name='twitter:${name}' content='${content}'>`, tw, ' ');
    return `${plainMetas}
            ${fbMetas}
            ${twMetas}`;
};


const createBody = ({contents, scripts = []}) => {
    return`
  <body>
    <div id='app'>
      ${contents}
    </div>
    ${createScripts(scripts)}
  </body>`;
};



const createHead = ({title, metas = [], links = []}) => {
    return `
  <head> 
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <title>${title}</title>
    ${createMetas(metas)}
    ${createLinks(links)}
  </head>`;
};



const createStaticFile = ({body = {}, head = {}}) => {
    return `
<!doctype html>
<html lang='en'>
  ${createHead(head)}
  ${createBody(body)}
</html>`;
};

module.exports.createStaticFile = createStaticFile;

const staticHead = {
    head: {
	title: 'test static',
	metas: {
	    plain: [
		{name: 'description', content: 'a test'},
		{name: 'author', content: 'foobar'},
		{name: 'keywords', content: 'test, some, more'},
		{name: 'copyright', content: 'foobar@2020'}
	    ],
	    fb: [
		{prop: 'title', content: 'test static'},
		{prop: 'type', content: 'foobar'},
		{prop: 'image', content: 'https://some.com/image.png'},
		{prop: 'url', content: 'https://some.com'},
		{prop: 'description', content: 'a test'}
	    ],
	    tw: [
		{name: 'card', content: 'summary'},
		{name: 'title', content: 'test static'},
		{name: 'image', content: 'https://some.com/image.png'},
		{name: 'description', content: 'a test'},
		{name: 'url', content: 'https://some.com'}
	    ]
	}
    }
};

module.exports.staticHead = staticHead;


/* const dummyData = {
 *     head: {
 * 	title: 'test static',
 * 	metas: {
 * 	    plain: [
 * 		{name: 'description', content: 'a test'},
 * 		{name: 'author', content: 'foobar'},
 * 		{name: 'keywords', content: 'test, some, more'},
 * 		{name: 'copyright', content: 'foobar@2020'}
 * 	    ],
 * 	    fb: [
 * 		{prop: 'title', content: 'test static'},
 * 		{prop: 'type', content: 'foobar'},
 * 		{prop: 'image', content: 'https://some.com/image.png'},
 * 		{prop: 'url', content: 'https://some.com'},
 * 		{prop: 'description', content: 'a test'}
 * 	    ],
 * 	    tw: [
 * 		{name: 'card', content: 'summary'},
 * 		{name: 'title', content: 'test static'},
 * 		{name: 'image', content: 'https://some.com/image.png'},
 * 		{name: 'description', content: 'a test'}
 * 	    ]
 * 	},
 * 	links: [ 'assets/css/style1.css', 'assets/css/style2.css', 'assets/css/style3.css' ]
 *     },
 *     body: {
 * 	contents: `<div><p>hello world!</p></div>`,
 * 	scripts: [ 'assets/js/js1.js', 'assets/js/js2.js', 'assets/js/js3.js' ]
 *     }
 * };*/

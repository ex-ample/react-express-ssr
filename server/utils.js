const fn = require('fn-js');
const path = require('path');
const fs = require('fs');


const extRegex = /.+\.(.+)$/;
const rName = /^(.+)\-?\..+$/;

const getExt = arg => {
    const r = arg.match(extRegex);
    return r ? r[1] : null;
};

const listFiles = (dirPath, level = 0) => {
    if (!fn.isNum(level)) throw new Error('level must be an Number type');
    const listX = fn.map(dirent => {
	const name = dirent.name;
	const fPath = path.resolve(dirPath, name);
	return fn.condNext(
	    {
		path: fPath,
		name: name,
		isSym: dirent.isSymbolicLink(),
	    },
	    [dirent.isDirectory(),               (arg) => fn.assoc(arg, "isDir", true)],
	    [dirent.isFile(),                    (arg) => fn.assoc(arg, 'ext', getExt(name))],
	    [level > 0 && dirent.isDirectory(),  (arg) => fn.assoc(arg, "subs", fn.trampoline(listFiles, fPath, level - 1))]
	);
    });
    return listX(fs.readdirSync(dirPath, {withFileTypes: true}));
};

module.exports.listFiles = listFiles;


const fileMapX = (fileMaps, basePath) => {
    return fn.reduce((init, {name, path}) => {
	const reName = name.match(rName);
	const baseName = reName ? reName[1] : name;
	const relPath = path.split(`${basePath}/`)[1];
	return fn.get_(init, baseName)
	     ? fn.updateIn(init, [baseName], fn.merge, relPath)
	     : fn.assoc(init, baseName, [relPath]);
    }, fileMaps, {});
};


module.exports.fileMapX = fileMapX;

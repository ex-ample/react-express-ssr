const fn = require('fn-js');
const path = require('path');
const fs = require('fs');
const { createStaticFile, staticHead } = require('./staticGen.js');
const { listFiles, fileMapX } = require('./utils.js');
const { staticRenderer } = require('./build/renderer.js');


const devMode = process.env.NODE_ENV !== 'production';
const staticDir = devMode ? path.resolve(process.cwd(), 'dev') : path.resolve(process.cwd(), 'dist');
const port = 3000;
const context = {};
const express = require('express');
const app = express();


const assets = fn.reduce((init, {name, subs}) => {
    return {...init, [name]: fileMapX(subs, staticDir)};
}, listFiles(`${staticDir}/assets`, 2), {});


app.use(express.static(staticDir, {
    extensions: ['js', 'css']
}));


app.get('/', (req, res, next) => {
    const contents = staticRenderer(req, context);
    const skel = fn.compose((arg) => fn.assocIn(arg, ["body", "contents"], contents),
			    (arg) => fn.assocIn(arg, ["body", "scripts"], fn.getIn(assets, ["js", "app"])));
    res.send(createStaticFile(skel(staticHead))).end();
});


app.get('/about', (req, res, next) => {
    const contents = staticRenderer(req, context);
    const skel = fn.compose((arg) => fn.assocIn(arg, ["body", "contents"], contents),
			    (arg) => fn.assocIn(arg, ["body", "scripts"], fn.getIn(assets, ["js", "app"])));
    res.send(createStaticFile(skel(staticHead))).end();
});


app.listen(port, () => console.log(`Example app listening on port ${port}!`));
